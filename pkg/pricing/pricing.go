package pricing

import "github.com/RoseSecurity/gke-node-viewer/pkg/model"

// Provider provides compute instance prices for display in the node viewer
type Provider interface {
	NodePrice(n *model.Node) (float64, bool)
	OnUpdate(onUpdate func())
}
