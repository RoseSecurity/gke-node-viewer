/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package model

import (
	"fmt"
	"strings"

	"github.com/charmbracelet/bubbles/progress"
	"github.com/charmbracelet/lipgloss"
)

type Style struct {
	white    func(strs ...string) string
	blue     func(strs ...string) string
	purple   func(strs ...string) string
	gradient progress.Option
}

func ParseStyle(style string) (*Style, error) {
	colors := strings.Split(style, ",")
	if len(colors) != 3 {
		return nil, fmt.Errorf("three colors must be provided for the style, found %d (%q)", len(colors), style)
	}
	s := &Style{}
	s.white = lipgloss.NewStyle().Foreground(lipgloss.Color(colors[0])).Render
	s.blue = lipgloss.NewStyle().Foreground(lipgloss.Color(colors[1])).Render
	s.purple = lipgloss.NewStyle().Foreground(lipgloss.Color(colors[2])).Render

	s.gradient = progress.WithGradient(colors[0], colors[2]) // This will still only create a gradient from white to purple
	return s, nil
}
