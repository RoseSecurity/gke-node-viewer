/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package gcp

import (
	"log"
	"sync"

	"cloud.google.com/go/compute/metadata"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/cloudbilling/v1"
	"google.golang.org/api/option"
)

// ...

func (p *pricingProvider) InstancePrice(instanceType string) (float64, bool) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	if val, ok := p.instancePrices[instanceType]; ok {
		return val, true
	}
	return 0.0, false
}

func (p *pricingProvider) updatePricing(ctx context.Context) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := p.updateInstancePricing(ctx); err != nil {
			log.Printf("updating instance pricing, %s, using existing pricing data", err)
		}
	}()
	wg.Wait()
}

func (p *pricingProvider) updateInstancePricing(ctx context.Context) error {
	// Get the project ID
	projectID, err := metadata.ProjectID()
	if err != nil {
		return err
	}

	// Create a new Cloud Billing service
	client, err := google.DefaultClient(ctx, cloudbilling.CloudPlatformScope)
	if err != nil {
		return err
	}
	service, err := cloudbilling.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		return err
	}

	// Get the list of services
	resp, err := service.Services.List().Do()
	if err != nil {
		return err
	}

	// Find the Compute Engine service
	for _, s := range resp.Services {
		if s.DisplayName == "Compute Engine" {
			// Get the pricing information for the service
			pricingInfo, err := service.Services.Skus.List(s.Name).Do()
			if err != nil {
				return err
			}

			// Update the instance prices
			p.mu.Lock()
			defer p.mu.Unlock()
			for _, sku := range pricingInfo.Skus {
				if sku.Category.ResourceFamily == "Compute" && sku.Category.ResourceGroup == "G1Small" {
					p.instancePrices[sku.Description] = sku.PricingInfo[0].PricingExpression.TieredRates[0].UnitPrice.Units
				}
			}
			break
		}
	}

	return nil
}
