# gke-node-viewer

## Getting Started

```sh
# Clone repository
git clone git@gitlab.com:RoseSecurity/gke-node-viewer.git
cd gke-node-viewer

# Build binary
make build

# Run
./gke-node-viewer
```

## Output

![gke-node-viewer](./.static/screenshot.png)
