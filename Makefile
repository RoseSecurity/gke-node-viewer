.PHONY: help clean boilerplate download generate

NO_COLOR=\033[0m
GREEN=\033[32;01m
YELLOW=\033[33;01m
RED=\033[31;01m

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[33m%-20s\033[0m %s\n", $$1, $$2}'

build: generate ## Build
	go build -ldflags="-s -w -X main.version=local -X main.builtBy=Makefile" ./cmd/gke-node-viewer

download: ## Download dependencies
	go mod download
	go mod tidy

boilerplate: ## Add license headers
	go run hack/boilerplate.go ./

clean: ## Clean artifacts
	rm -rf gke-node-viewer
	rm -rf dist/